<?php

  class Grupo extends CI_Model
    {
      function __construct()
      {
          parent::__construct();
      }
      public function insertar($datos){
        //ponemos el nombre de la tabla de la BDD CREADO
        return $this->db->insert("grupo_bt",$datos);
      }
      // consulta de todos los estudiantes d ela base de datos
      public function obtenerTodos(){
        // linea de consulta
        $grupos=$this->db->get('grupo_bt');
        if ($grupos->num_rows()>0) {
          return $grupos;
        } else {
          return false; //cuando no haya datos
        }
      }
      // funcion para eliminar estudiantes se necesito el id
      public function eliminarPorId($id){
        $this->db->where("id_gru_bt",$id);
        return $this->db->delete('grupo_bt');
      }//
    // nuevo
      // consultando datos por id para actualizar la knformacion
      public function obtenerPorId($id){
        $this->db->where("id_gru_bt",$id);
        $grupos=$this->db->get("grupo_bt");
        if ($grupos->num_rows()>0) {
          return $grupos->row();//retorna datos
        } else {
          return false;
        }
      }
      //procso para actualizar
      public function actualizar($id,$datos){
        $this->db->where("id_gru_bt",$id);
        return $this->db->update("grupo_bt",$datos);
    }

  }//cierre de la clase no borrar
