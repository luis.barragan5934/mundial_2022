<?php

  class Equipo extends CI_Model
    {
      function __construct()
      {
          parent::__construct();
      }
      public function insertar($datos){
        //ponemos el nombre de la tabla de la BDD CREADO
        return $this->db->insert("equipo_bt",$datos);
      }
      // consulta de todos los estudiantes d ela base de datos
      public function obtenerTodos(){
        // linea de consulta
        $equipos=$this->db->get('equipo_bt');
        if ($equipos->num_rows()>0) {
          return $equipos;
        } else {
          return false; //cuando no haya datos
        }
      }
      // funcion para eliminar estudiantes se necesito el id
      public function eliminarPorId($id){
        $this->db->where("id_equ_bt",$id);
        return $this->db->delete('equipo_bt');
      }//
    // nuevo
      // consultando datos por id para actualizar la knformacion
      public function obtenerPorId($id){
        $this->db->where("id_equ_bt",$id);
        $equipos=$this->db->get("equipo_bt");
        if ($equipos->num_rows()>0) {
          return $equipos->row();//retorna datos
        } else {
          return false;
        }
      }
      //procso para actualizar
      public function actualizar($id,$datos){
        $this->db->where("id_equ_bt",$id);
        return $this->db->update("equipo_bt",$datos);
      }

  }//cierre de la clase no borrar
