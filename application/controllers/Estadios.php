<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadios extends CI_Controller {
	public function __construct(){
		parent::__construct();
		// llamamos al modelo estudiante singular
			$this->load->model("estadio");

	}

// renderisamos la vista index estudiantes, presenta en el navegador la vist

	public function index()
	{
		$data["listadoEstadios"]=$this->estadio->obtenerTodos();
		$this->load->view('header');
		$this->load->view('estadios/index',$data);
		$this->load->view('footer');
	}
  // creamos nueva funcion con el NUEVO.php en vistas
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('estadios/nuevo');
		$this->load->view('footer');
  }


//funcion para capturar valores
	public function guardarEstadios(){
		$datosNuevoEstadios=array(
			"nom_est_bt"=>$this->input->post('nom_est_bt'),
			"dir_est_bt"=>$this->input->post('dir_est_bt')
		);
		print_r($datosNuevoEstadios);
		if ($this->estadio->insertar($datosNuevoEstadios)) {
			$this->session->set_flashdata('confirmacion','Estudiante ingresado correctamente');
		}else{
			$this->session->set_flashdata('Error','Error al imgresar, intente nuevamente');
		}
		redirect('estadios/index');
	}
///FUNCION para celiminar el estudiante echo en el modelo
	public function borrar($id_est_bt){
		if ($this->estadio->eliminarPorId($id_est_bt)) {
			// code...
			redirect('estadios/index');
		} else {
			// code...
			echo "ERROR AL ELIMINAR :()";
		}
	}
	//funcion para renderizar formulario de actualizar
	public function actualizar($id){
		$data["estadiosEditar"]=$this->estadio->obtenerPorId($id);
	    $this->load->view('header');
		$this->load->view('estadios/actualizar',$data);
		$this->load->view('footer');
	}
	public function procesarActualizar(){
		$datosEstadiosEditado=array(
			"nom_est_bt"=>$this->input->post('nom_est_bt'),
			"dir_est_bt"=>$this->input->post('dir_est_bt'),
		);
		$id=$this->input->post("id_est_bt");

		if ($this->estadio->actualizar($id,$datosEstadiosEditado)) {
			redirect('estadios/index');
		}else{
			echo "EROOR";
		}
	}
}//NO BORRAR
