<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller {
	public function __construct(){
		parent::__construct();
		// llamamos al modelo estudiante singular
			$this->load->model("noticia");

	}

  public function index(){
    $this->load->view('header');
		$this->load->view('noticias/index');
		$this->load->view('footer');
  }
}