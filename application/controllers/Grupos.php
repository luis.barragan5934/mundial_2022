<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		// llamamos al modelo singular
			$this->load->model("grupo");
	}

	public function index()
	{
		$data["listaGrupos"]=$this->grupo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupos/index',$data);
		$this->load->view('footer');
	}
  // creamos nueva funcion con el NUEVO.php en vistas
  public function nuevo(){
    	$this->load->view('header');
		$this->load->view('grupos/nuevo');
		$this->load->view('footer');
    }

//funcion para capturar valores
	public function guardarGrupos(){
		$datosNuevoGrupos=array(
			"nom_gru_bt"=>$this->input->post('nom_gru_bt'),
			"equ_gru_bt"=>$this->input->post('equ_gru_bt')			
		);
		print_r($datosNuevoGrupos);
		if ($this->grupo->insertar($datosNuevoGrupos)) {
			$this->session->set_flashdata('confirmacion','Estudiante ingresado correctamente');
		}else{
			$this->session->set_flashdata('Error','Error al imgresar, intente nuevamente');
		}
		redirect('grupos/index');		
	}
///FUNCION para eliminar el estudiante echo en el modelo
	public function borrar($id_gru_bt){
		if ($this->grupo->eliminarPorId($id_gru_bt)) {
			redirect('grupos/index');
		} else {
			echo "ERROR AL ELIMINAR :(";
		}
	}

	public function actualizar($id){
		$data["gruposEditar"]=$this->grupo->obtenerPorId($id);
    	$this->load->view('header');
		$this->load->view('grupos/actualizar',$data);
		$this->load->view('footer');
  }

	public function procesarActualizar(){
		$datosGruposEditados=array(
			"nom_gru_bt"=>$this->input->post('nom_gru_bt'),
			"equ_gru_bt"=>$this->input->post('equ_gru_bt')
		);
		$id=$this->input->post("id_gru_bt");
		if ($this->grupo->actualizar($id,$datosGruposEditados)) {
			redirect('grupos/index');
		}else{
			echo "EROOR";
		}
	}
}
