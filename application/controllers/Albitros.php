<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Albitros extends CI_Controller {
	public function __construct(){
		parent::__construct();
		// llamamos al modelo estudiante singular
			$this->load->model("albitro");

	}

// renderisamos la vista index estudiantes, presenta en el navegador la vist

	public function index()
	{
		$data["listadoAlbitros"]=$this->albitro->obtenerTodos();
		$this->load->view('header');
		$this->load->view('albitros/index',$data);
		$this->load->view('footer');
	}
  // creamos nueva funcion con el NUEVO.php en vistas
  public function nuevo(){
    $this->load->view('header');
	$this->load->view('albitros/nuevo');
	$this->load->view('footer');
  }


//funcion para capturar valores
	public function guardarAlbitros(){
		$datosNuevoAlbitros=array(
			"ced_alb_bt"=>$this->input->post('ced_alb_bt'),
			"ape_alb_bt"=>$this->input->post('ape_alb_bt'),
			"nom_alb_bt"=>$this->input->post('nom_alb_bt'),
			"tel_alb_bt"=>$this->input->post('tel_alb_bt'),
			"dir_alb_bt"=>$this->input->post('dir_alb_bt'),
			"fec_nac_alb_bt"=>$this->input->post('fec_nac_alb_bt')

		);

		//proceso de subida de fotos
		$this->load->library("upload");//activamos la ñibreria para subir archivo
	    $new_name = "foto_". time() . "_" . rand(1, 5000);//generamos nombre aleatorio para que no se repita la misma foto
	    $config['file_name'] = $new_name;
	    $config['upload_path'] = FCPATH . 'uploads/albitros/';//ruta de subida
	    $config['allowed_types'] = 'jpg|png';//tipo de archivo que subamos jpg---pdf/docs
	    $config['max_size'] = 5*1024 ;
	    $this->upload->initialize($config);//inicializamos la configuracion no cambiamos nada
			// validamos la subida del archivo con el for
	    if ($this->upload->do_upload("fot_alb_bt")) {
	      $dataSubida = $this->upload->data();
	      $datosNuevoAlbitros["fot_alb_bt"] = $dataSubida['file_name'];
	    }
			//final del proceso de subidad de arichivos
		print_r($datosNuevoAlbitros);
		if ($this->albitro->insertar($datosNuevoAlbitros)) {
			// echo "OK";
			redirect('albitros/index');
		}else{
			echo "EROOR";
			}
		}
///FUNCION para celiminar el estudiante echo en el modelo
	public function borrar($id_alb_bt){
		if ($this->albitro->eliminarPorId($id_alb_bt)) {
			// code...
			redirect('albitros/index');
		} else {
			// code...
			echo "ERROR AL ELIMINAR";
		}
	}
	//funcion para renderizar formulario de actualizar
	public function actualizar($id){
			$data["albitrosEditar"]=$this->albitro->obtenerPorId($id);
	    $this->load->view('header');
			$this->load->view('albitros/actualizar',$data);
			$this->load->view('footer');
	  }
		public function procesarActualizacion(){
			$datosAlbitrosEditado=array(
				"ced_alb_bt"=>$this->input->post('ced_alb_bt'),
				"ape_alb_bt"=>$this->input->post('ape_alb_bt'),
				"nom_alb_bt"=>$this->input->post('nom_alb_bt'),
				"tel_alb_bt"=>$this->input->post('tel_alb_bt'),
				"dir_alb_bt"=>$this->input->post('dir_alb_bt'),
				"fec_nac_alb_bt"=>$this->input->post('fec_nac_alb_bt')
			);
			$id=$this->input->post("id_est");

			if ($this->albitro->actualizar($id,$datosAlbitrosEditado)) {
				// echo "OK";
				redirect('albitros/index');
			}else{
				echo "EROOR";
				}
			}
}//NO BORRAR
