<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		// llamamos al modelo singular
			$this->load->model("equipo");
	}

	public function index()
	{
		$data["listaEquipos"]=$this->equipo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('equipos/index',$data);
		$this->load->view('footer');
	}
  // creamos nueva funcion con el NUEVO.php en vistas
    public function nuevo(){
    	$this->load->view('header');
		$this->load->view('equipos/nuevo');
		$this->load->view('footer');
    }

//funcion para capturar valores
	public function guardarEquipos(){
		$datosNuevoEquipos=array(
			"nom_equ_bt"=>$this->input->post('nom_equ_bt'),
			"des_equ_bt"=>$this->input->post('des_equ_bt')			
		);
		$this->load->library("upload");//activamos la ñibreria para subir archivo
		$new_name = "foto_". time() . "_" . rand(1, 5000);//generamos nombre aleatorio para que no se repita la misma foto
		$config['file_name'] = $new_name;
		$config['upload_path'] = FCPATH . 'uploads/equipos/';//ruta de subida
		$config['allowed_types'] = 'jpg|png';//tipo de archivo que subamos jpg---pdf/docs
		$config['max_size'] = 5*1024 ;
		$this->upload->initialize($config);//inicializamos la configuracion no cambiamos nada
			// validamos la subida del archivo con el for
		if ($this->upload->do_upload("fot_equ_bt")) {
			$dataSubida = $this->upload->data();
			$datosNuevoEquipos["fot_equ_bt"] = $dataSubida['file_name'];
		}//fin del proceso de subida d earchivos, fotos
		else{
			print_r($this->upload->display_errors());
		}
		print_r($datosNuevoEquipos);
		if ($this->equipo->insertar($datosNuevoEquipos)) {
			$this->session->set_flashdata('confirmacion','Estudiante ingresado correctamente');
		}else{
			$this->session->set_flashdata('Error','Error al ingresar, intente nuevamente');
		}
		redirect('equipos/index');
	}
///FUNCION para eliminar el estudiante echo en el modelo
	public function borrar($id_equ_bt){
		if ($this->equipo->eliminarPorId($id_equ_bt)) {
			redirect('equipos/index');
		} else {
			echo "ERROR AL ELIMINAR :(";
		}
	}

	public function actualizar($id){
		$data["equiposEditar"]=$this->equipo->obtenerPorId($id);
    	$this->load->view('header');
		$this->load->view('equipos/actualizar',$data);
		$this->load->view('footer');
    }

	public function procesarActualizar(){
		$datosEquiposEditados=array(
			"nom_equ_bt"=>$this->input->post('nom_equ_bt'),
			"des_equ_bt"=>$this->input->post('des_equ_bt')
		);
		$id=$this->input->post("id_equ_bt");
		if ($this->equipo->actualizar($id,$datosEquiposEditados)) {
			redirect('equipos/index');
		}else{
			echo "EROOR";
		}
	}
}
