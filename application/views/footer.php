<div class="row" style="background-color:#1A9BF2 ;">
  <div class="col-md-12 text-center">
  <h5 style="color:white">FIFA BT PARTNERS</h5>
</div></div>
<div class="row text-center" style="background-color:#1A9BF2;" >
  <div class="col-md-2">
  <a href="https://www.coca-cola.com/" target="_blank" >
  <img src="<?php echo base_url()?>/assets/images/footer1.jpg" height="20%" width="100px" alt="logo">
  </a></div>
  <div class="col-md-2">
  <a href="https://www.latin-america.adidas.com/storefinder#/" target="_blank" >
  <img src="<?php echo base_url()?>/assets/images/footer2.jpg" height="15%" width="80px" alt="logo">
  </a></div>
  <div class="col-md-2">
  <a href="https://www.hyundaimotorgroup.com/main/mainRecommend" target="_blank" >
  <img src="<?php echo base_url()?>/assets/images/footer3.jpg" height="25%" width="100px" alt="logo">
  </a></div>
  <div class="col-md-2">
  <a href="https://www.budweiser.com/en" target="_blank" >
  <img src="<?php echo base_url()?>/assets/images/footer4.jpg" height="25%" width="100px" alt="logo">
  </a></div>
  <div class="col-md-2">
  <a href="https://www.claro.com/" target="_blank" >
  <img src="<?php echo base_url()?>/assets/images/footer5.jpg" height="20%" width="100px" alt="logo">
  </a></div>
  <div class="col-md-2">
  <a href="http://www.mengniuir.com/tc/index.aspx" target="_blank" >
  <img src="<?php echo base_url()?>/assets/images/footer6.jpg" height="20%" width="100px" alt="logo">
  </a></div>
</div>
<div class="row" style="background-color:#0B456D ;">
  <div class="col-md-9">
        <h6 style="color:white">Ⓒ 2022 - Todos los derechos reservados - DESAROLLADO POR: BARRAGAN LUIS & TAPIA JOSEPH</h6>
  </div>
  <div class="col-md-3">
        <h6 style="color:white">Aviso legal</h6>
  </div>
</div>
<style>
  label.error{
      border:1px solid white;
      color:white;
      background-color:#E15B69;
      padding:5px;
      padding-left:15px;
      padding-right:15px;
      font-size:12px;
      opacity: 0;        
        left: 0px;
        transform: translate(0, 10px);
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
        width: auto;
        margin-top:30px !important;
        z-index: 10;
        opacity: 1;
        visibility: visible;
        transform: translate(0, -20px);
        transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
        border-radius:10px;
        width:100%;

  }

  input.error{
      border:1px solid #E15B69;
  }


  select.error{
      border:1px solid #E15B69;
  }

  label.error:before{
      position: absolute;
        z-index: -1;
        content: "";
        right: calc(90% - 10px);
        top: -8px;
        border-style: solid;
        border-width: 0 10px 10px 10px;
        border-color: transparent transparent #E15B69 transparent;
        transition-duration: 0.3s;
        transition-property: transform;
}
</style>
<?php if ($this->session->flashdata('confirmacion')): ?>
        <script type="text/javascript">
          $(document).ready(function(){
            Swal.fire(
              'CONFIRMACIÓN', //titulo
              '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
              'success' //Tipo de alerta
            )
          });
        </script>
      <?php endif; ?>


      <?php if ($this->session->flashdata('error')): ?>
        <script type="text/javascript">
          $(document).ready(function(){
            Swal.fire(
              'ERROR', //titulo
              '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
              'error' //Tipo de alerta
            )
          });
        </script>
      <?php endif; ?>
</body>
</html>
