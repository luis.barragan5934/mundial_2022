<legend>
        <div  style="background-color:#1A499C ;" class="row">
          <h1 class="text-center">
            <i class="glyphicon glyphicon-level-up"></i>
            <b style="color:white";>ACTUALIZAR ALBITRO</b>
          </h1>
        </div>
      </legend>
      <center>
      <a href="<?php echo site_url('albitros/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-12">
    <?php if ($albitrosEditar): ?>
      <form  class="" action="<?php echo site_url('Albitros/procesarActualizacion') ?>" method="post">
        <input type="hidden" name="id_alb_bt" value="<?php echo $albitrosEditar->id_alb_bt; ?>">
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Cedula</label>
          </div>
          <div class="col-md-7">
            <input type="number" name="ced_alb_bt" value="<?php echo $albitrosEditar->ced_alb_bt; ?>"
            class="form-control" placeholder="Ingrese su Cedula" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Apellido</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="ape_alb_bt" value="<?php echo $albitrosEditar->ape_alb_bt; ?>"
            class="form-control" placeholder="Ingrese la Apellido" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Nombre</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="nom_alb_bt" value="<?php echo $albitrosEditar->nom_alb_bt; ?>"
            class="form-control" placeholder="Ingrese sus Nombres" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Telefono</label>
          </div>
          <div class="col-md-7">
            <input type="number" name="tel_alb_bt" value="<?php echo $albitrosEditar->tel_alb_bt; ?>"
            class="form-control" placeholder="Ingrese el numero de teleno" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Direccion</label>
          </div>
          <div class="col-md-7">
            <input type="text" name="dir_alb_bt" value="<?php echo $albitrosEditar->dir_alb_bt; ?>"
            class="form-control" placeholder="Ingrese su Direccion" required>
          </div>
        </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Fecha de Nacimiento</label>
        </div>
        <div class="col-md-7">
          <input type="date" name="fec_nac_alb_bt" value="<?php echo $albitrosEditar->fec_nac_alb_bt; ?>"
          class="form-control" placeholder="Ingrese su fecha de Nacimiento" required>
        </div>
      </div>
      <br>
      <div class="col-md-4 text-right">
        <label for="">Fotografia</label>
      </div>
      <div class="col-md-7">
        <input type="file" name="fot_alb_bt" value="<?php echo $albitrosEditar->fot_alb_bt; ?>"
        class="form-control" placeholder="Ingrese su fecha de Nacimiento" required>
      </div>
    </div>
    <br>
      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-7 text-center">
          <!-- cambianos el nombre de button a submit -->
          <button type="submit" name="button"
              class="btn btn-warning">
          <i class="glyphicon glyphicon-ok"></i>
          Actualizar
          </button>
          <a href="<?php echo site_url('albitros/index') ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            Cancelar
          </a>
        </div>
      </div>
      </form>
      <br><br>
    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO ALBITROS</b>
      </div>
    <?php endif; ?>
  </div>
</div>