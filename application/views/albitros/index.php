<legend>
    <div  style="background-color:#1A499C ;" class="row">
    <h1 class="text-center">
      <i class="glyphicon glyphicon-paste"></i>
      <b style="color:white";>GESTION DE ALBITROS</b>
    </h1>
    </div>
    <br>
    <center>
      <a  href="<?php echo site_url('albitros/nuevo'); ?>" class="btn btn-success">
        <i class="glyphicon glyphicon-pencil">
          AGREGAR
        </i>
      </a>
    </center>
  </legend>
  <br>
  <br>
  <br>
  <?php if ($listadoAlbitros): ?>
  <table id="tbl_estudiantes" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        
        <th class="text-center">CEDULA</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">FECHA NACIMIENTO</th>
        <th class="text-center">FOTOGRAFIA</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php if ('listadoAlbitros'): ?>

      <?php foreach ($listadoAlbitros->result() as $estudianteTemporal): ?>
        <tr>

 
        <td class="text-center"><?php echo $estudianteTemporal->ced_alb_bt;?></td>
        <td class="text-center"><?php echo $estudianteTemporal->ape_alb_bt;?></td>
        <td class="text-center"><?php echo $estudianteTemporal->nom_alb_bt;?></td>
        <td class="text-center"><?php echo $estudianteTemporal->tel_alb_bt;?></td>
        <td class="text-center"><?php echo $estudianteTemporal->dir_alb_bt;?></td>
        <td class="text-center"><?php echo $estudianteTemporal->fec_nac_alb_bt; ?></td>
        <td class="text-center"><?php if ($estudianteTemporal->fot_alb_bt!=""): ?>
                    <a href="<?php echo base_url('uploads/albitros').'/'.$estudianteTemporal->fot_alb_bt; ?>"
                      target="_blank">
                      <img src="<?php echo base_url('uploads/albitros').'/'.$estudianteTemporal->fot_alb_bt; ?>"
                      width="50px" height="50px"
                      alt="">
                    </a>
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
        </td>
        <td class="text-center">
          <a href="<?php echo site_url('albitros/actualizar');?>/<?php echo $estudianteTemporal->id_alb_bt;?>" class="btn btn-warning">
            <i class="glyphicon glyphicon-edit"></i>
              Editar
          </a>
          <a href="<?php echo site_url('albitros/borrar');?>/<?php echo $estudianteTemporal->id_alb_bt;?>" class="btn btn-danger" onclick="return confirm('ESTAS PENDEJO')">
            <i class="glyphicon glyphicon-trash"></i>
              Eliminar
          </a>
        </td>
      </tr>
      <?php endforeach; ?>
      <?php endif; ?>
    </tbody>

  </table>
<?php else: ?>
  <h3><b>NO EXISTEN ESTUDIANTES</b></h3>
  <!-- letra b bols de negrita -->
<?php endif; ?>
<script type="text/javascript">
$("#tbl_estudiantes").DataTable();
</script> 