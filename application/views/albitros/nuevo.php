<legend>
  <div  style="background-color:#1A499C ;" class="row">
    <h1 class="text-center">
      <i class="glyphicon glyphicon-level-up"></i>
      <b style="color:white";>NUEVOS ALBITROS</b>
    </h1>
  </div>
</legend>
<form id="frm_nuevo_estudiante" class=""
enctype="multipart/form-data"
action="<?php echo site_url('Albitros/guardarAlbitros') ?>" method="post">
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Cedula</label>
    </div>
    <div class="col-md-7">
      <input id="ced_alb_bt" type="number" name="ced_alb_bt" value="" class="form-control" placeholder="Ingrese su Cedula" >
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Apellido</label>
    </div>
    <div class="col-md-7">
      <!-- el tipo de comentarios -->
      <input id="ape_alb_bt" type="text" name="ape_alb_bt" value="" class="form-control" placeholder="Ingrese la Apellido" >
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Nombre</label>
    </div>
    <div class="col-md-7">
      <!-- el tipo de comentarios -->
      <input id="nom_alb_bt" type="text" name="nom_alb_bt" value="" class="form-control" placeholder="Ingrese sus Nombres" >
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Telefono</label>
    </div>
    <div class="col-md-7">
      <input id="tel_alb_bt" type="number" name="tel_alb_bt" value="" class="form-control" placeholder="Ingrese el numero de teleno" >
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Direccion</label>
    </div>
    <div class="col-md-7">
      <input id="dir_alb_bt" type="text" name="dir_alb_bt" value="" class="form-control" placeholder="Ingrese su Direccion" >
    </div>
  </div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fecha de Nacimiento</label>
  </div>
  <div class="col-md-7">
    <input id="fec_nac_alb_bt" type="date" name="fec_nac_alb_bt" value="" class="form-control" placeholder="Ingrese su fecha de Nacimiento" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fotografia</label>
  </div>
  <div class="col-md-7">
    <input id="fot_alb_bt" type="file" name="fot_alb_bt" value="" class="form-control" placeholder="Ingrese su fotografia" accept="image/*" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <!-- cambianos el nombre de button a submit -->
    <button type="submit" name="button"
        class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
    Guardar
    </button>
    <a href="<?php echo site_url('albitros/index') ?>" class="btn btn-danger">
      <i class="glyphicon glyphicon-remove"></i>
      Cancelar
    </a>
  </div>
</div>
</form>
<script type="text/javascript">
  $("#frm_nuevo_estudiante").validate({
    rules:{
      ape_alb_bt:{
        required:true,
        minlength:3
      },
      ced_alb_bt:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      nom_alb_bt:{
        required:true,
        minlength:3
      },
      tel_alb_bt:{
        required:true,
        minlength:9,
        maxlength:9,
        digits:true
      },
      dir_alb_bt:{
        required:true,
        minlength:10
      },
      fec_nac_alb_bt:{
        required:true
      },
      fot_alb_bt:{
        required:true
      },
    },
    messages:{
      ape_alb_bt:{
        required:"INGRESE LOS APELLIDOS",
        minlength:"APELLIDO INCORRECTO"
      },
      ced_alb_bt:{
        required:"INGRESE LA CEDULA",
        minlength:"CEDULA INCORRECTA",
        maxlength:"CEDULA INCORRECTA",
        digits:"SOLO INGRESE NUMEROS"
      },
      nom_alb_bt:{
        required:"INGRESE LOS NOMBRES",
        minlength:"NOMBRE INCORRECTO"
      },
      tel_alb_bt:{
        required:"INGRESE EL NUMERO DE TELEFONO",
        minlength:"TELEFONO INCORRECTO",
        maxlength:"TELEFONO INCORRECTO",
        digits:"SOLO INGRESE NUMEROS"
      },
      dir_alb_bt:{
        required:"INGRESE LA DIRECCION",
        minlength:"DIRECCION INCORRECTA"
      },
      fec_nac_alb_bt:{
        required:"INGRESE LA FECHA"
      },
      fot_alb_bt:{
        required:"INGRESE LA FOTOGRAFIA"
      },
    }
  });
</script>