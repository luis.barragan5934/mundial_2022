<legend>
    <div  style="background-color:#1A499C ;" class="row">
    <h1 class="text-center">
      <i class="glyphicon glyphicon-paste"></i>
      <b style="color:white";>GESTION ESTADIOS</b>
    </h1>
    </div>
    <br>
    <center>
      <a href="<?php echo site_url('estadios/nuevo') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Nuevo
      </a>
    </center>
</legend>
<br>
<br>
<br>
<?php if ($listadoEstadios): ?>
  <table id="tbl_estadio" class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">DIRECCION</th>
          <th class="text-center">ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php if ('listadoEstadios'): ?>
        <?php foreach ($listadoEstadios->result() as $equipoTemporal): ?>        
          <tr>
        
            <td class="text-center"><?php echo $equipoTemporal->nom_est_bt;?></td>
            <td class="text-center"><?php echo $equipoTemporal->dir_est_bt;?></td>
            <td class="">  
               
              <a href="<?php echo site_url('estadios/actualizar');?>/<?php echo $equipoTemporal->id_est_bt;?>" class="btn btn-warning">
              <i class="glyphicon glyphicon-edit"></i>
              Editar
              </a>
              <a href="<?php echo site_url('estadios/borrar');?>/<?php echo $equipoTemporal->id_est_bt;?>" class="btn btn-danger" onclick="return confirm('DESEAS ELIMINAR'); ">
              <i class="glyphicon glyphicon-trash"></i>
              Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
  </table>
<?php else: ?>
  <h3><b>NO EXISTEN ESTADIOS</b></h3>
  <!-- letra b bols de negrita -->
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_estadio").DataTable();
</script>