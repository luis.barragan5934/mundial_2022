<legend class="text-center">
    NUEVOS ESTADIOS
</legend>

<!-- primero cambiamos el button al sutmit  luego al action del form -->
<!-- agregamos en enctype para que me cargue las fotos o nunca va a valer -->
<form  id="frm_nuevo_equipo" class="" enctype="multipart/form-data" action="<?php echo site_url('estadios/guardarEstadios'); ?>" method="post">
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Nombre</label>
    </div>
    <div class="col-md-7">
      <!-- el tipo de comentarios -->
      <input type="text" id="nom_est_bt" name="nom_est_bt" value=""
      class="form-control" placeholder="Ingrese sus Nombres">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Direccion</label>
    </div>
    <div class="col-md-7">
      <input type="text" id="dir_est_bt" name="dir_est_bt" value=""
      class="form-control" placeholder="Ingrese su Direccion">
    </div>
  </div>
<br>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <!-- cambianos el nombre de button a submit -->
    <button type="submit" name="button"
        class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
    Guardar
    </button>
    <a href="<?php echo site_url('estadios/index') ?>" class="btn btn-danger">
      <i class="glyphicon glyphicon-remove"></i>
      Cancelar
    </a>
  </div>
</div>
</form>

<!-- creamos un nuevo scrip para capturar objeto html por su id gracias a jquery validate -->

<script type="text/javascript">
  $("#frm_nuevo_equipo").validate({
    rules:{
      // definimos reglas de validacion para ingreso de datos
      nom_est_bt:{
        required:true,
        minlength:4
      },
      dir_est_bt:{
        required:true,
        minlength:10
      }

    },
    messages:{
      nom_est_bt:{
        required:"Porfavor ingrese un nombre",
        minlength:"Nombre incorrecto"
      },
      dir_est_bt:{
        required:"Porfavor complete el campo",
        minlength:"Direccion incorrecta"
      }

    }
  });
</script>