<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXV6fJ3AmkKcvYoxYWcAyQ5Y1ddJwSD68&libraries=places&callback=initMap" >
    </script>
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_AR.min.js'); ?>"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

     <!-- Additional CSS Files -->
     <link rel="stylesheet" href="https://ciya.utc.edu.ec/ciya//assets/presentacion/css/fontawesome.css">
     <link rel="stylesheet" href="https://ciya.utc.edu.ec/ciya//assets/presentacion/css/templatemo-grad-school.css">
     <link rel="stylesheet" href="https://ciya.utc.edu.ec/ciya//assets/presentacion/css/owl.css">
     <link rel="stylesheet" href="https://ciya.utc.edu.ec/ciya//assets/presentacion/css/lightbox.css">

        <!-- importacion sweetalert2 -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js"></script>



    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MUNDIAL</title>

    <!-- importacion sweetalert2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


  </head>
  <body>
    <div class="row" style="background-color:#0B456D ;">
      <div class="col-md-2">
      <img src="<?php echo base_url()?>/assets/images/header1.jpg" height="20%" width="100px" alt="logo">
      <div class="col-md-10">
      </div>
      </div>
    </div>
    <div class="row text-center" style="background-color:#1A9BF2;" >
      <div class="col-md-2">
        <br>
      <img src="<?php echo base_url()?>/assets/images/header2.jpg" height="20%" width="100px" alt="logo">
      </div>
      <div class="col-md-9">
        <nav id="menu" class="main-nav" role="navigation">
          <ul class="main-menu">
            <li ><a href="<?php echo site_url() ?>">INICIO</a></li>
            <li ><a href="<?php echo site_url('noticias/index') ?>">NOTICIAS</a></li>
            <li class="submenu"><a href="">ESTADIOS</a>
              <ul class="sub-menu" style="background-color:#0B456D ;">
                <li><a href="<?php echo site_url('estadios/index') ?>"  style="color:white;">LISTADO</a></li>
                <li><a href="<?php echo site_url('estadios/nuevo') ?>"  style="color:white;">AGREGAR</a></li>
              </ul>
            </li>
            <li class="submenu"><a href="">ALBITROS</a>
              <ul class="sub-menu" style="background-color:#0B456D ;">
                <li><a href="<?php echo site_url('albitros/index') ?>" style="color:white;">LISTADO</a></li>
                <li><a href="<?php echo site_url('albitros/nuevo') ?>" style="color:white;">NUEVO</a></li>
              </ul>
            </li>
            <li class="submenu"><a href="">EQUIPOS</a>
              <ul class="sub-menu" style="background-color:#0B456D ;">
                <li><a href="<?php echo site_url('equipos/index') ?>" style="color:white;">LISTADO</a></li>
                <li><a href="<?php echo site_url('equipos/nuevo') ?>" style="color:white;">NUEVO</a></li>
              </ul>
            </li>
            <li class="submenu"><a href="">GRUPOS</a>
              <ul class="sub-menu" style="background-color:#0B456D ;">
                <li><a href="<?php echo site_url('grupos/index') ?>" style="color:white;">LISTADO</a></li>
                <li><a href="<?php echo site_url('grupos/nuevo') ?>" style="color:white;">NUEVO</a></li>
              </ul>
            </li>
            <li class="submenu"><a href="">MARCADOR</a>
              <ul class="sub-menu" style="background-color:#0B456D ;">
                <li><a href="" style="color:white;">LISTADO</a></li>
                <li><a href="" style="color:white;">NUEVO</a></li>
              </ul>
            </li>
            <li class="submenu"><a href="">TABLA</a>
              <ul class="sub-menu" style="background-color:#0B456D ;">
                <li><a href="" style="color:white;">LISTADO</a></li>
                <li><a href="" style="color:white;">NUEVO</a></li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
      <div class="col-md-1">
        <br>
        <ul class="nav nav-pills" >
          <li role="presentation" class="active"><a href="https://store.fifa.com/es-fr" target="_blank">FIFA Store</a></li>
        </ul>
    </div>
  </div>
