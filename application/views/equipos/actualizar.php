<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR EQUIPOS</h3>
    <center>
      <a href="<?php echo site_url('equipos/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($equiposEditar): ?>
      <!-- <?php print_r($equiposEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('Equipos/procesarActualizar') ?>" method="post">
        <center>
          <input type="hidden" name="id_equ_bt" value="<?php echo $equiposEditar->id_equ_bt; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>
        <br>
          <div class="row">
            <div class="col-md-4 text-right">
            <label for="">Nombre</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text"  name="nom_equ_bt" value="<?php echo $equiposEditar->nom_equ_bt; ?> " class="form-control" placeholder="Ingrese sus Nombres">
          </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">Descripcion</label>
          </div>
          <div class="col-md-7">
            <input type="text"  name="des_equ_bt" value="<?php echo $equiposEditar->des_equ_bt; ?> "
            class="form-control" placeholder="Ingrese su Direccion">
          </div>
          </div>
          <br>
        <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Fotografia</label>
        </div>
        <div class="col-md-7">
          <input type="file"  name="fot_equ_bt" value="<?php echo $equiposEditar->fot_equ_bt; ?> " required accept="image/*"> <!-- ponemos required accep y definimos el formato de imagen o documento que vayamos a subir -->
        </div>
      </div>
        <br>
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-7">
            <!-- cambianos el nombre de button a submit -->
            <button type="submit" name="button"
                class="btn btn-primary">
            <i class="glyphicon glyphicon-ok"></i>
            Guardar
            </button>
            <a href="<?php echo site_url('equipos/index') ?>" class="btn btn-danger">
              <i class="glyphicon glyphicon-remove"></i>
              Cancelar
            </a>
          </div>
        </div>    

      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO NINGUN EQUIPO</b>
      </div>
    <?php endif; ?>
  </div>
</div>
