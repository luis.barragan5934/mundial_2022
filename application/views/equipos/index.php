<legend class="text-center">
    GESTION DE EQUIPOS
    <center>
      <a href="<?php echo site_url('equipos/nuevo') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Nuevo
      </a>
    </center>
</legend>
<br>
<br>
<br>
<?php if ($listaEquipos): ?>
  <table id="tbl_equipos" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
      
        <th class="text-center">NOMBRE</th>
        <th class="text-center">DESCRIPCION</th>
        <th class="text-center">FOTOGRAFIA</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php if ('listaEquipos'): ?>

      <?php foreach ($listaEquipos->result() as $equipoTemporal): ?>
        <tr>       
        <td class="text-center"><?php echo $equipoTemporal->nom_equ_bt;?></td>
        <td class="text-center"><?php echo $equipoTemporal->des_equ_bt;?></td>        
        <td class="text-center">
          <?php if ($equipoTemporal->fot_equ_bt!=""): ?>
            <a href="<?php echo base_url('uploads/equipos').'/'.$equipoTemporal->fot_equ_bt;?>"
            target="_blank">
            <img src="<?php echo base_url('uploads/equipos').'/'.$equipoTemporal->fot_equ_bt;?>"
            width:"50px" height="50px"
           </a>
          <?php else: ?>
            N/A
          <?php endif; ?>
        </td>
        <td class="">
          <a href="<?php echo site_url('equipos/actualizar');?>/<?php echo $equipoTemporal->id_equ_bt;?>" class="btn btn-warning">
            <i class="glyphicon glyphicon-edit"></i>
              Editar
          </a>
          <a href="<?php echo site_url('equipos/borrar');?>/<?php echo $equipoTemporal->id_equ_bt;?>" class="btn btn-danger" onclick="return confirm('DESEAS ELIMINAR'); ">
            <i class="glyphicon glyphicon-trash"></i>
              Eliminar
          </a>
        </td>
      </tr>
      <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3><b>NO EXISTEN EQUIPOS</b></h3>
  <!-- letra b bols de negrita -->
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_equipos").DataTable();
</script>

