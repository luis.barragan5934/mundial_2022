<legend class="text-center">
    NUEVOS EQUIPOS.
</legend>

<!-- primero cambiamos el button al sutmit  luego al action del form -->
<!-- agregamos en enctype para que me cargue las fotos o nunca va a valer -->
<form  id="frm_nuevo_equipo" class="" enctype="multipart/form-data" action="<?php echo site_url('equipos/guardarEquipos'); ?>" method="post">
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Nombre</label>
    </div>
    <div class="col-md-7">
      <!-- el tipo de comentarios -->
      <input type="text" id="nom_equ_bt" name="nom_equ_bt" value=""
      class="form-control" placeholder="Ingrese sus Nombres">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Descripcion</label>
    </div>
    <div class="col-md-7">
      <input type="text" id="des_equ_bt" name="des_equ_bt" value=""
      class="form-control" placeholder="Ingrese su Direccion">
    </div>
  </div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fotografia</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="fot_equ_bt" name="fot_equ_bt" value="" required accept="image/*"> <!-- ponemos required accep y definimos el formato de imagen o documento que vayamos a subir -->
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <!-- cambianos el nombre de button a submit -->
    <button type="submit" name="button"
        class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
    Guardar
    </button>
    <a href="<?php echo site_url('equipos/index') ?>" class="btn btn-danger">
      <i class="glyphicon glyphicon-remove"></i>
      Cancelar
    </a>
  </div>
</div>
</form>

<!-- creamos un nuevo scrip para capturar objeto html por su id gracias a jquery validate -->

<script type="text/javascript">
  $("#frm_nuevo_equipo").validate({
    rules:{
      // definimos reglas de validacion para ingreso de datos
      nom_equ_bt:{
        required:true,
        minlength:4
      },
      des_equ_bt:{
        required:true,
        minlength:10
      }

    },
    messages:{
      nom_equ_bt:{
        required:"Porfavor ingrese un nombre",
        minlength:"Nombre incorrecto"
      },
      des_equ_bt:{
        required:"Porfavor complete el campo",
        minlength:"Direccion incorrecta"
      }

    }
  });
</script>
