<legend class="text-center">
    NUEVOS GRUPOS
</legend>

<!-- primero cambiamos el button al sutmit  luego al action del form -->
<!-- agregamos en enctype para que me cargue las fotos o nunca va a valer -->
<form  id="frm_nuevo_grupo" class="" enctype="multipart/form-data" action="<?php echo site_url('grupos/guardarGrupos'); ?>" method="post">
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">Nombre</label>
    </div>
    <div class="col-md-7">
      <!-- el tipo de comentarios -->
      <input type="number" id="nom_gru_bt" name="nom_gru_bt" value="" class="form-control" placeholder="Ingrese  Nombre">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 text-right">
        <label for="">EQUIPO</label>
    </div>
    <div class="col-md-7">
      <select class"form-control" type="text" id="equ_gru_bt" name="equ_gru_bt"  class="form-control input-sm " required> <option value="">--selecione--</option>
          <option value"PRIMERO">Primero</option>
          <option value="SEGUNDO">Segundo</option>
          <option value"TERCERO">Tercero</option>
          <option value="CUARTO">Cuarto</option>
      </select>
    </div>
  </div>  
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <!-- cambianos el nombre de button a submit -->
    <button type="submit" name="button"
        class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
    Guardar
    </button>
    <a href="<?php echo site_url('grupos/index') ?>" class="btn btn-danger">
      <i class="glyphicon glyphicon-remove"></i>
      Cancelar
    </a>
  </div>
</div>
</form>

<!-- creamos un nuevo scrip para capturar objeto html por su id gracias a jquery validate -->

<script type="text/javascript">
  $("#frm_nuevo_grupo").validate({
    rules:{
      // definimos reglas de validacion para ingreso de datos
      nom_gru_bt:{
        required:true,
        minlength:1
      }

    },
    messages:{
      nom_equ_bt:{
        required:"Porfavor ingrese un nombre",
        minlength:"Nombre incorrecto"
      }

    }
  });
</script>
