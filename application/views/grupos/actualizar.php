<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR GRUPOS</h3>
    <center>
      <a href="<?php echo site_url('grupos/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($gruposEditar): ?>
      <!-- <?php print_r($gruposEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('Grupos/procesarActualizar') ?>" method="post">
        <center>
          <input type="hidden" name="id_gru_bt" value="<?php echo $gruposEditar->id_gru_bt; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>
        <br>
          <div class="row">
            <div class="col-md-4 text-right">
            <label for="">Nombre</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="number"  name="nom_gru_bt" value="<?php echo $gruposEditar->nom_gru_bt; ?> " class="form-control" placeholder="Ingrese sus Nombres">
          </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
                <label for="">EQUIPO</label>
            </div>
            <div class="col-md-7">
              <select class"form-control" type="text" id="equ_gru_bt" name="equ_gru_bt"  class="form-control input-sm " required> <option value="">--selecione--</option>
                  <option value"PRIMERO">Primero</option>
                  <option value="SEGUNDO">Segundo</option>
                  <option value"TERCERO">Tercero</option>
                  <option value="CUARTO">Cuarto</option>
                </select>
              </div>
            </div>
          <br>
        <br>
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-7">
            <!-- cambianos el nombre de button a submit -->
            <button type="submit" name="button"
                class="btn btn-primary">
            <i class="glyphicon glyphicon-ok"></i>
            Guardar
            </button>
            <a href="<?php echo site_url('grupos/index') ?>" class="btn btn-danger">
              <i class="glyphicon glyphicon-remove"></i>
              Cancelar
            </a>
          </div>
        </div>    

      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO NINGUN EQUIPO</b>
      </div>
    <?php endif; ?>
  </div>
</div>
