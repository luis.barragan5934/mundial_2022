<legend class="text-center">
    GESTION DE GRUPOS
    <center>
      <a href="<?php echo site_url('grupos/nuevo') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Nuevo
      </a>
    </center>
</legend>
<br>
<br>
<br>
<?php if ($listaGrupos): ?>
  <table id="tbl_grupos" class="table table-striped table-bordered table-hover">
    <thead>
      <tr class="text-center">
        <th class="text-center">ID</th>
        <th class="text-center">NUMERO GRUPO</th>
        <th class="text-center">EQUIPO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody>
        <?php if ('listaGrupos'): ?>

        <?php foreach ($listaGrupos->result() as $grupoTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $grupoTemporal->id_gru_bt;?></td>
            <td class="text-center"><?php echo $grupoTemporal->nom_gru_bt;?></td>
            <td class="text-center"><?php echo $grupoTemporal->equ_gru_bt;?></td>
            
            <td class="text-center">
              <a href="<?php echo site_url('grupos/actualizar');?>/<?php echo $grupoTemporal->id_gru_bt;?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                  Editar
              </a>
              <a href="<?php echo site_url('grupos/borrar');?>/<?php echo $grupoTemporal->id_gru_bt;?>" class="btn btn-danger" onclick="return confirm('DESEAS ELIMINAR'); ">
                <i class="glyphicon glyphicon-trash"></i>
                  Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3><b>NO EXISTEN GRUPOS</b></h3>
  <!-- letra b bols de negrita -->
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_grupos").DataTable();
</script>

